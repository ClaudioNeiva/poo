package br.ucsal.bes20172.poo.aula14;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DataExemplo {

	private static Scanner scanner = new Scanner(System.in);

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public static void main(String[] args) {
		exemplificarData();
	}

	private static void exemplificarData() {
		Date data1 = obterData();
		System.out.println("Data informada=" + sdf.format(data1));
	}

	private static Date obterData() {
		String dataNascimentoString;
		Date dataNascimento = null;
		do {
			System.out.print("Informe sua data de nascimento (dd/mm/yyyy):");
			dataNascimentoString = scanner.nextLine();
			try {
				dataNascimento = sdf.parse(dataNascimentoString);
			} catch (ParseException e) {
				System.out.println("Data inv�lida!");
			}
		} while (dataNascimento == null);
		return dataNascimento;
	}

}
