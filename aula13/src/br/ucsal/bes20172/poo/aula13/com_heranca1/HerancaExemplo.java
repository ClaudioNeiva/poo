package br.ucsal.bes20172.poo.aula13.com_heranca1;

import java.util.Date;

public class HerancaExemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica1 = new PessoaFisica();

		pessoaFisica1.cpf = "123";
		pessoaFisica1.nomeMae = "Maria";
		pessoaFisica1.dataNascimento = new Date();

		pessoaFisica1.nome = "Pedro";

		Endereco endereco = new Endereco();
		endereco.logradouro = "Rua y";
		endereco.numero = "123";
		endereco.bairro = "Bairro X";
		endereco.cidade = "Salvador";
		endereco.uf = "BA";

		pessoaFisica1.endereco = endereco;
	}

}
