package br.ucsal.bes20172.poo.aula13.sem_heranca2;

import java.util.Date;

public class Pessoa {
	
	private String cpf;

	private String nome;

	private Endereco endereco;

	private String nomeMae;

	private Date dataNascimento;

	private String cnpj;
	
	private String inscEstadual;
	
	private String inscMunicipal;
	
	private Boolean tributacaoSimples;
	
	private String telefone;

}
