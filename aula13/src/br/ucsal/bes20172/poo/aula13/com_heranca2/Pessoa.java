package br.ucsal.bes20172.poo.aula13.com_heranca2;

public abstract class Pessoa {

	private String nome;

	private Endereco endereco;

	public Pessoa(String nome) {
		this.nome = nome;
//		System.out.println("Passei pelo construtor da PESSOA(nome)...");
	}

	public Pessoa(String nome, Endereco endereco) {
		this.nome = nome;
		this.endereco = endereco;
//		System.out.println("Passei pelo construtor da PESSOA(nome, endereco)...");
	}

	public abstract void apresentar();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
