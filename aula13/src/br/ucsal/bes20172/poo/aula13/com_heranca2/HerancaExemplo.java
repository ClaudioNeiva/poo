package br.ucsal.bes20172.poo.aula13.com_heranca2;

import java.util.Date;

public class HerancaExemplo {

	public static void main(String[] args) {
		// while (true) {
		// try {
		exemplificarHeranca();
		// } catch (Exception e) {
		// // Logar o erro
		// System.out.println("ERRO! Contacte o suporte (cod
		// 4e5r6t78yyyyyhdhfhfh).");
		// }
		// }
	}

	private static void exemplificarHeranca() {
		PessoaFisica pessoaFisica1 = new PessoaFisica("Maria", new Endereco(), "123123", "Clara", new Date());

		pessoaFisica1.apresentar();

		PessoaJuridica pessoaJuridica1 = new PessoaJuridica("Apple", new Endereco(), "79789789");

		pessoaJuridica1.apresentar();

		apresentarPessoa(pessoaFisica1);

		apresentarPessoa(pessoaJuridica1);

		Pessoa pessoaA = new PessoaFisica("Jo�o", new Endereco(), "678768", "Leila", new Date());

		System.out.println("pessoa1.nome=" + pessoaA.getNome());

		PessoaFisica pessoaFisicaA = (PessoaFisica) pessoaA;

		System.out.println("pessoaFisicaA.cpf=" + pessoaFisicaA.getCpf());

		if (pessoaA instanceof PessoaJuridica) {
			PessoaJuridica pessoaJuridicaA = (PessoaJuridica) pessoaA;

			System.out.println("pessoaJuridicaA.cnpj=" + pessoaJuridicaA.getCnpj());
		} else {
			System.out.println(
					"A vari�vel pessoaA n�o aponta para uma inst�ncia de PessoaJuridica, n�o sendo poss�vel realizar o cast.");
		}

		// N�o � poss�vel instanciar a classe Pessoa, pois a mesma foi definida
		// como abstrata.
		// Pessoa pessoa2 = new Pessoa("Mateus");
		//
		// pessoa2.apresentar();
	}

	private static void apresentarPessoa(Pessoa pessoa) {
		pessoa.apresentar();
		if (pessoa instanceof PessoaFisica) {
			PessoaFisica pf1 = (PessoaFisica) pessoa;
			System.out.println("Convers�o poss�vel, o cpf � " + pf1.getCpf());
		} else {
			System.out.println(
					"Pessoa incompat�vel com PessoaFisica. Classe de pessoa=" + pessoa.getClass().getCanonicalName());
		}
	}

	// pessoaFisica1.setNome("Maria");
	// pessoaFisica1.setEndereco(new Endereco());
	// pessoaFisica1.setCpf("12312");
	// pessoaFisica1.setNomeMae("Clara");
	// pessoaFisica1.setDataNascimento(new Date());

}
