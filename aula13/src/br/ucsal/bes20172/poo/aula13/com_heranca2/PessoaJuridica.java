package br.ucsal.bes20172.poo.aula13.com_heranca2;

public class PessoaJuridica extends Pessoa {

	private String cnpj;

	private String inscEstadual;

	private String inscMunicipal;

	private Boolean tributacaoSimples;

	public PessoaJuridica(String nome, String cnpj, String inscEstadual, String inscMunicipal,
			Boolean tributacaoSimples) {
		super(nome);
		this.cnpj = cnpj;
		this.inscEstadual = inscEstadual;
		this.inscMunicipal = inscMunicipal;
		this.tributacaoSimples = tributacaoSimples;
	}

	public PessoaJuridica(String nome, Endereco endereco, String cnpj, String inscEstadual, String inscMunicipal,
			Boolean tributacaoSimples) {
		super(nome, endereco);
		this.cnpj = cnpj;
		this.inscEstadual = inscEstadual;
		this.inscMunicipal = inscMunicipal;
		this.tributacaoSimples = tributacaoSimples;
	}

	public PessoaJuridica(String nome, Endereco endereco, String cnpj) {
		super(nome, endereco);
		this.cnpj = cnpj;
	}

	@Override
	public void apresentar() {
		// super.apresentar();
		System.out.println("Bom dia, eu sou a empresa " + getNome() + ", CNPJ " + cnpj + ".");
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscEstadual() {
		return inscEstadual;
	}

	public void setInscEstadual(String inscEstadual) {
		this.inscEstadual = inscEstadual;
	}

	public String getInscMunicipal() {
		return inscMunicipal;
	}

	public void setInscMunicipal(String inscMunicipal) {
		this.inscMunicipal = inscMunicipal;
	}

	public Boolean getTributacaoSimples() {
		return tributacaoSimples;
	}

	public void setTributacaoSimples(Boolean tributacaoSimples) {
		this.tributacaoSimples = tributacaoSimples;
	}

}
