package br.ucsal.bes20172.poo.aula13.com_heranca2;

import java.util.Date;

public class PessoaFisica extends Pessoa {

	private String cpf;

	private String nomeMae;

	private Date dataNascimento;

	public PessoaFisica(String nome, Endereco endereco, String cpf, String nomeMae, Date dataNascimento) {
		super(nome, endereco);
		this.cpf = cpf;
		this.nomeMae = nomeMae;
		this.dataNascimento = dataNascimento;
		// System.out.println("Passei pelo construtor de PESSOAFISICA...");
	}

	@Override
	public void apresentar() {
		// super.apresentar();
		System.out.println("Bom dia, meu nome � " + getNome() + ", filho de " + nomeMae + ".");
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
