package br.ucsal.bes20172.poo.aula13.sem_heranca1;

public class PessoaJuridica {

	private String cnpj;
	
	private String nome;
	
	private Endereco endereco;
	
	private String inscEstadual;
	
	private String inscMunicipal;
	
	private Boolean tributacaoSimples;
	
	private String telefone;
}
