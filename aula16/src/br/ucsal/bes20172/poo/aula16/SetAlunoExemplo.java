package br.ucsal.bes20172.poo.aula16;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SetAlunoExemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Set<Aluno> alunos = new HashSet<>();

		System.out.println("Informe 3 alunos distintos:");

		do {
			Aluno aluno = obterAluno();
			alunos.add(aluno);
		} while (alunos.size() != 3);

		System.out.println("Os nomes informados foram:");
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

	}

	private static Aluno obterAluno() {
		Integer matricula;
		String nome;
		String email;
		Aluno aluno;
		System.out.print("Matrícula:");
		matricula = scanner.nextInt();
		scanner.nextLine();
		System.out.print("Nome:");
		nome = scanner.nextLine();
		System.out.print("Email:");
		email = scanner.nextLine();
		aluno = new Aluno(matricula, nome, email);
		return aluno;
	}

}
