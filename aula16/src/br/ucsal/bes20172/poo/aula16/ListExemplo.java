package br.ucsal.bes20172.poo.aula16;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListExemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();
		List<Integer> qtdRepeticoes = new ArrayList<>();

		System.out.println("Informe 5 nomes:");

		for (int j = 0; j < 5; j++) {
			String nome = scanner.nextLine();
			if (nomes.contains(nome)) {
				int i = nomes.indexOf(nome); // retorna a posi��o do objeto dentro da lista
				int qtdRepeticoesAtual = qtdRepeticoes.get(i);
				qtdRepeticoes.set(i, qtdRepeticoesAtual + 1); // substitui um valor presente na posi��o i da lista
			} else {
				nomes.add(nome);
				qtdRepeticoes.add(1);
			}
		}

		System.out.println("Os nomes e quantas vezes foram informados:");
		for (int i = 0; i < nomes.size(); i++) {
			System.out.println(nomes.get(i) + " = " + qtdRepeticoes.get(i) + " vezes");
		}

	}

}
