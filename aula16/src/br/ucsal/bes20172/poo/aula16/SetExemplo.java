package br.ucsal.bes20172.poo.aula16;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class SetExemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Set<String> nomes = new HashSet<>();
		// Set<String> nomes = new LinkedHashSet<>();
		// Set<String> nomes = new TreeSet<>();
		
		System.out.println("Informe 3 nomes distintos:");

		do {
			nomes.add(scanner.nextLine());
		} while (nomes.size() != 3);

		System.out.println("Os nomes informados foram:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

	}

}
