package br.ucsal.bes20172.poo.aula16;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListQtdExemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		System.out.println("Informe 3 nomes distintos:");

		do {
			String nome = scanner.nextLine();
			if (!nomes.contains(nome)) {
				nomes.add(nome);
			}
		} while (nomes.size() != 3);

		System.out.println("Os nomes informados foram:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

	}

}
