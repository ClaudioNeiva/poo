package br.ucsal.bes20172.poo.aula16;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapExemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		// Map<nome, qtd-repeticoes>
		Map<String, Integer> nomesQtd = new HashMap<>();

		System.out.println("Informe 5 nomes:");
		for (int j = 0; j < 5; j++) {
			String nome = scanner.nextLine();
			if (nomesQtd.containsKey(nome)) {
				int qtdRepeticoesAtual = nomesQtd.get(nome);
				nomesQtd.put(nome, qtdRepeticoesAtual + 1);
			} else {
				nomesQtd.put(nome, 1);
			}
		}

		System.out.println("Os nomes e quantas vezes foram informados:");
		for (String nome : nomesQtd.keySet()) {
			System.out.println(nome + " = " + nomesQtd.get(nome));
		}

	}

}
