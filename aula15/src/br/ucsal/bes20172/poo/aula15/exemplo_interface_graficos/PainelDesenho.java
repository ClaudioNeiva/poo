package br.ucsal.bes20172.poo.aula15.exemplo_interface_graficos;

import java.util.ArrayList;
import java.util.List;

public class PainelDesenho {

	private List<FormaGeometrica> formasGeometricas = new ArrayList<>();

	public void adcionar(FormaGeometrica formaGeometrica) {
		formasGeometricas.add(formaGeometrica);
	}

	public void desenhar() {
		for(FormaGeometrica formaGeometrica : formasGeometricas){
			formaGeometrica.desenhar();
		}
	}

}
