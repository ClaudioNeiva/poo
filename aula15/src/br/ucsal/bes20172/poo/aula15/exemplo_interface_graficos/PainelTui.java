package br.ucsal.bes20172.poo.aula15.exemplo_interface_graficos;

import java.util.Scanner;

public class PainelTui {

	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		executarPainel();
	}

	private static void executarPainel() {
		PainelDesenho painel = new PainelDesenho();
		while(true){
			
			System.out.println("Informe o tipo de objeto (Q=quadrado; T=triangulo; C=circulo):");
			String tipoObjeto = scanner.nextLine();
			
			if(tipoObjeto.equalsIgnoreCase("Q")){
				painel.adcionar(new Quadrado());
			}else if(tipoObjeto.equalsIgnoreCase("T")){
				painel.adcionar(new Triangulo());
			}else if(tipoObjeto.equalsIgnoreCase("C")){
				painel.adcionar(new Circulo());
			}
			
			painel.desenhar();
		}
	}
	
}
