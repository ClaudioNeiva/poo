package br.ucsal.bes20172.poo.aula15.exemplo_interface_graficos;

public class Circulo implements FormaGeometrica {
	
	@Override
	public void desenhar() {
		System.out.println("   ####  ");
		System.out.println(" ##    ## ");
		System.out.println("#        #");
		System.out.println("#        #");
		System.out.println(" ##    ## ");
		System.out.println("   ####  ");
	}

}
