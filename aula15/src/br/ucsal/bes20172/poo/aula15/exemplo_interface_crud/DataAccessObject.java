package br.ucsal.bes20172.poo.aula15.exemplo_interface_crud;

public interface DataAccessObject {

	void inserir(Object obj);

	void excluir(Object obj);

	void excluir(Integer chave);

	void atualizar(Object obj);

	Object consultar(Integer chave);

}
