package br.ucsal.bes20172.poo.aula15.exemplo_interface_ordenacao;

public class AlunoEspecializacao extends Aluno implements Comparable<AlunoEspecializacao> {

	private String nomeInstituicaoGraduacao;

	public AlunoEspecializacao(String nome, String telefone, Integer matricula, Integer anoIngresso,
			String nomeInstituicaoGraduacao) {
		super(nome, telefone, matricula, anoIngresso);
		this.nomeInstituicaoGraduacao = nomeInstituicaoGraduacao;
	}

	public String getNomeInstituicaoGraduacao() {
		return nomeInstituicaoGraduacao;
	}

	public void setNomeInstituicaoGraduacao(String nomeInstituicaoGraduacao) {
		this.nomeInstituicaoGraduacao = nomeInstituicaoGraduacao;
	}

	@Override
	public int compareTo(AlunoEspecializacao o) {
		return nomeInstituicaoGraduacao.compareTo(o.nomeInstituicaoGraduacao);
	}

}
