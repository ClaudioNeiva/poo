package br.ucsal.bes20172.poo.aula15.exemplo_interface_ordenacao;

public class PessoaFisica {

	private String nome;

	private String telefone;

	public PessoaFisica(String nome, String telefone) {
		super();
		this.nome = nome;
		this.telefone = telefone;
	}

	public void exibirTelefoneFormatado() {
		System.out.println("(" + telefone.substring(0, 2) + ")" + telefone.substring(2));
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
