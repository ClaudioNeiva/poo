package br.ucsal.bes20172.poo.aula15.exemplo_interface_ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExemploUsoHeranca {

	public static void main(String[] args) {
		AlunoEspecializacao alunoEspecializacao = new AlunoEspecializacao("claudio", "7112345678", 567, 2015, "UCSal");
		alunoEspecializacao.exibirTelefoneFormatado();
		
		List<AlunoEspecializacao> alunosEspecializacao = new ArrayList<>();
		
		Collections.sort(alunosEspecializacao);
		
		List<Integer> numeros = new ArrayList<>();
		Collections.sort(numeros);
		
		ordenar(alunoEspecializacao, alunoEspecializacao);
	}
	
	public static void ordenar(Object objeto1, Object objeto2){
		((Comparador)objeto1).comparar(objeto2);
	}

}
