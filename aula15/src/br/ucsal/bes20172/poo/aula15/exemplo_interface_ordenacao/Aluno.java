package br.ucsal.bes20172.poo.aula15.exemplo_interface_ordenacao;

public class Aluno extends PessoaFisica {

	private Integer matricula;

	private Integer anoIngresso;

	public Aluno(String nome, String telefone, Integer matricula, Integer anoIngresso) {
		super(nome, telefone);
		this.matricula = matricula;
		this.anoIngresso = anoIngresso;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Integer getAnoIngresso() {
		return anoIngresso;
	}

	public void setAnoIngresso(Integer anoIngresso) {
		this.anoIngresso = anoIngresso;
	}

}
