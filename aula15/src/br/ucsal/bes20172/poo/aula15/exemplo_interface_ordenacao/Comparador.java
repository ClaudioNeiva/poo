package br.ucsal.bes20172.poo.aula15.exemplo_interface_ordenacao;

public interface Comparador {

	int comparar(Object outroObjeto);
	
}
